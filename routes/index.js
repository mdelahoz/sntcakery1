var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.user) {
    return res.redirect('/orders');
  }
  var vm = {
  	title: 'Toronto Custom Cakes | Sweet n Tasty Cakery',
  	page: 'home',
  	error: req.flash('error')
  }

  res.render('default', vm);
});

module.exports = router;
